class LoginResponse {
  int code;
  String accesstoken;
  String message;
  int step;

  LoginResponse({this.code, this.accesstoken, this.step});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    accesstoken = json['accesstoken'];
    step = json['step'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['accesstoken'] = this.accesstoken;
    data['step'] = this.step;
    data['message'] = this.message;
    return data;
  }

  @override
  String toString() {
    return 'LoginResponse{code: $code, accesstoken: $accesstoken, message: $message, step: $step}';
  }
}
