class Endpoint {
  int code;
  String endpoint;

  Endpoint({this.code, this.endpoint});

  Endpoint.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    endpoint = json['endpoint'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['endpoint'] = this.endpoint;
    return data;
  }
}
