import 'package:demo_app/page/home/home_controller.dart';
import 'package:demo_app/page/qr_code/qr_code_controller.dart';
import 'package:demo_app/widgets/custom_button.dart';
import 'package:demo_app/widgets/custom_textfield.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QrCodeScreen extends StatefulWidget {
  const QrCodeScreen({Key key}) : super(key: key);

  @override
  _QrCodeScreenState createState() => _QrCodeScreenState();
}

class _QrCodeScreenState extends State<QrCodeScreen> {
  TextEditingController _qrController = TextEditingController();
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode result;
  QRViewController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _qrController.text = "demo qr code";
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: 30,
              ),
              CustomButton(
                width: 200,
                height: 50,
                text: "SCAN QR",
                onClick: () {
                  Get.dialog(QRView(
                    key: qrKey,
                    onQRViewCreated: _onQRViewCreated,
                  ));
                },
              ),
              SizedBox(
                height: 30,
              ),
              Obx(
                () => Text(
                  "Result Scan QR: ${Get.find<QrCodeController>().qrCode.value}",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              CustomButton(
                width: 200,
                height: 50,
                text: "GENERATE QR",
                onClick: () {
                  setState(() {});
                },
              ),
              SizedBox(
                height: 30,
              ),
              CustomTextField(
                labelText: "INPUT VALUE QR",
                textEditingController: _qrController,
                width: 300,
                height: 50,
              ),
              SizedBox(
                height: 30,
              ),
              QrImage(
                data: _qrController.text,
                version: QrVersions.auto,
                size: 300.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      Get.find<QrCodeController>().qrCode.value = scanData.code;
      Get.back();
    });
  }
}
