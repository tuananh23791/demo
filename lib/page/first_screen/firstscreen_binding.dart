import 'package:demo_app/controller/endpoint_controller.dart';
import 'package:demo_app/page/blank_screen/blank_screen_controller.dart';
import 'package:get/get.dart';

class FirstScreenBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(InfoController(),permanent: true);
    Get.put(BlankScreenController(),permanent: true);
  }
}