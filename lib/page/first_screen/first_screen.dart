import 'package:demo_app/controller/endpoint_controller.dart';
import 'package:demo_app/struct/base_stateful_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FirstScreen extends StatefulWidget {
  @override
  _FirstScreenState createState() => _FirstScreenState();
}

class _FirstScreenState extends BaseStatefulWidgetState<FirstScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => loadEndpoint());
  }

  loadEndpoint() async {
    await Get.find<InfoController>().getEndPoint();
  }

  @override
  String appBarTitle() {
    return "First Screen";
  }

  @override
  Widget buildWidget() {
    return Container(
        child: Center(
      child: Obx(() => Text(Get.find<InfoController>().status.value)),
    ));
  }

  @override
  bool isCanGoBack() {
    return false;
  }
}