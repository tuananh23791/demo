import 'package:demo_app/struct/base_widget_stateless.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import 'blank_screen_controller.dart';

class BlankScreen extends BaseStatelessWidget{
  @override
  String appBarTitle() {
    return Get.find<BlankScreenController>().title;
  }

  @override
  Widget buildWidget(BuildContext buildContext) {
    return Container(padding:EdgeInsets.all(30),child: Center(child: Text(Get.find<BlankScreenController>().content),));
  }

  @override
  bool isCanGoBack() {
    return true;
  }
}