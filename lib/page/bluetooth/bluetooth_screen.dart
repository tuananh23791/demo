import 'dart:convert';

import 'package:demo_app/page/bluetooth/bluetooth_controller.dart';
import 'package:demo_app/utils/utils.dart';
import 'package:demo_app/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:get/get.dart';

class BluetoothScreen extends StatefulWidget {
  const BluetoothScreen({Key key}) : super(key: key);

  @override
  _BluetoothScreenState createState() => _BluetoothScreenState();
}

class _BluetoothScreenState extends State<BluetoothScreen> {
  FlutterBlue flutterBlue = FlutterBlue.instance;
  List<String> listDeviceName = <String>[];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    flutterBlue.scanResults.listen((results) {
      // do something with scan results
      for (ScanResult r in results) {
        if(r.device.name.length > 0 && !listDeviceName.contains(r.device.name)){
          listDeviceName.add(r.device.name);
          Get.find<BluetoothController>().listScanResult.add(r);
        }
        print('${r.device.name} found! rssi: ${r.rssi}');
      }
    },onDone:() => _done());

    flutterBlue.connectedDevices
        .asStream()
        .listen((List<BluetoothDevice> devices) {
      for (BluetoothDevice device in devices) {
        print("connected:::::${device.name}");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            CustomButton(
              width: 200,
              height: 50,
              text: "SCAN BLUETOOTH",
              onClick: () {
                listDeviceName.clear();
                Get.find<BluetoothController>().listScanResult.clear();
                flutterBlue.startScan(timeout: Duration(seconds: 5));
                // Utils().showLoading();
              },
            ),
            SizedBox(
              height: 30,
            ),
            Expanded(child: listScanResult()),
          ],
        ),
      ),
    );
  }

  _done(){
    print("done roi ne");
  }

  Widget listScanResult() {
    return Obx(() => ListView.builder(
          itemCount: Get.find<BluetoothController>().listScanResult.length,
          itemBuilder: (context, index) => GestureDetector(
            onTap: (){
              Get.find<BluetoothController>()
                  .listScanResult[index]
                  .device.connect();
              _discoverService(Get.find<BluetoothController>()
                  .listScanResult[index]
                  .device);
            },
            child: Container(
              height: 50,
              child: Text(Get.find<BluetoothController>()
                  .listScanResult[index]
                  .device
                  .name),
            ),
          ),
        ));
  }

  _discoverService(BluetoothDevice device) async {
    List<BluetoothService> services = await device.discoverServices();
    services.forEach((service) async {
      print("service::::::::${service.deviceId}");
      var characteristics = service.characteristics;
      for(BluetoothCharacteristic c in characteristics) {
        List<int> value = await c.read();
        Utils().showPopupDataMessage(message:json.encode(value));
      }
      // do something with service
    });
  }
}
