import 'package:demo_app/page/bluetooth/bluetooth_controller.dart';
import 'package:demo_app/page/home/home_controller.dart';
import 'package:demo_app/page/pdf_screen/pdf_controller.dart';
import 'package:demo_app/page/qr_code/qr_code_controller.dart';
import 'package:get/get.dart';

class HomeBindings extends Bindings {
  @override
  void dependencies() {
    Get.put(HomeController(), permanent: true);
    Get.put(QrCodeController());
    Get.put(BluetoothController());
    Get.put(PdfController());
  }
}
