import 'package:demo_app/page/bluetooth/bluetooth_screen.dart';
import 'package:demo_app/page/home/home_controller.dart';
import 'package:demo_app/page/login/login_screen.dart';
import 'package:demo_app/page/pdf_screen/pdf_screen.dart';
import 'package:demo_app/page/qr_code/qr_code_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Get.find<HomeController>().id.value = 0;
    Get.find<HomeController>().name.value = "OTP";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Obx(() => Text(Get.find<HomeController>().name.value)),
        centerTitle: true,
      ),
      body: Obx(() => getWidget()),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Drawer Header'),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            itemDrawer("OTP", 0),
            itemDrawer("Bluetooth", 1),
            itemDrawer("QR Code", 2),
            itemDrawer("PDF", 3),
          ],
        ),
      ),
    );
  }

  Widget itemDrawer(String title, int id) {
    return Obx(
      () => GestureDetector(
        onTap: () {
          Get.find<HomeController>().id.value = id;
          Get.find<HomeController>().name.value = title;
          Navigator.pop(context);
        },
        child: Container(
          height: 50,
          color: Get.find<HomeController>().id.value == id
              ? Colors.black26
              : Colors.white,
          child: Center(
            child: Text(title),
          ),
        ),
      ),
    );
  }

  Widget getWidget() {
    switch (Get.find<HomeController>().id.value) {
      case 1:
        return BluetoothScreen();
      case 2:
        return QrCodeScreen();
      case 3:
        return PdfScreen();
      default:
        return LoginScreen();
    }
  }
}
