import 'dart:async';

import 'package:demo_app/page/login/login_controller.dart';
import 'package:demo_app/struct/base_stateful_widget.dart';
import 'package:demo_app/struct/base_widget_stateless.dart';
import 'package:demo_app/utils/strings.dart';
import 'package:demo_app/utils/utils.dart';
import 'package:demo_app/widgets/custom_button.dart';
import 'package:demo_app/widgets/custom_textfield.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'login_widget.dart';
class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends BaseStatefulWidgetState<LoginScreen> {

  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _otpController = TextEditingController();
  bool isShowOtp = false;
  var _verificationId;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  String appBarTitle() {
    return "Login";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _phoneController.text = "0123456789";
    });
  }

  @override
  Widget buildWidget() {
    return Container(
      padding: EdgeInsets.all(30),
      child: SizedBox.expand(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 5,
              ),
              //text field email
              CustomTextField(
                height: 70,
                labelText: "Phone number",
                textEditingController: _phoneController,
                isShowClearText: true,
                textInputType: TextInputType.phone,
              ),
              SizedBox(
                height: 20,
              ),

              //text field password
              isShowOtp ? CustomTextField(
                height: 70,
                labelText: "OTP",
                textEditingController: _otpController,
                textInputType: TextInputType.number,
                isShowClearText: true,
              ) : SizedBox(),
              isShowOtp ? Row(children: [
                Text("$minute:$seconds"),
              ],) : SizedBox(),
              SizedBox(
                height: 20,
              ),

              //forgot password
              GestureDetector(
                onTap: () => Get.find<LoginController>().gotoBlankScreen(
                    title: "Forgot Password", content: "Forgot Password"),
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(forgot_password)),
              ),
              SizedBox(
                height: 30,
              ),

              //button login
              Row(
                children: [
                  Expanded(
                      child: CustomButton(
                        height: 70,
                        text: login,
                        onClick: _sendSms,
                      )),
                ],
              ),
              SizedBox(
                height: 20,
              ),

              //register
              Text(not_a_member,
                  style: TextStyle(color: Colors.black, fontSize: 17)),
              widgetRegister(),
            ],
          ),
        ),
      ),
    );
  }

  @override
  bool isCanGoBack() {
    return false;
  }

  _validation(){
    RegExp regExp = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+",);
    String errorMessage = "";
    bool emailValid = regExp.hasMatch(_phoneController.text);
    if(!emailValid){
      errorMessage = invalid_email;
    }else if(_otpController.text.isBlank){
      errorMessage = password_not_null;
    }

    if(!errorMessage.isBlank){
      Utils().showPopupErrorMessage(message: errorMessage);
    }else{
      _requestLogin();
    }
  }

  _requestLogin(){
    String email = _phoneController.text;
    String password = _otpController.text;
    Get.find<LoginController>()
        .login(email: email, password: password);
  }

  String minute = '00';
  String seconds = '00';
  bool isOtpExpire = false;
  Timer _timer;
  final strFormat = new NumberFormat("00", "en_US");
  void startTimer() {
    int timeExpired = 60;
      const oneSec = const Duration(seconds: 1);
      _timer = new Timer.periodic(
        oneSec,
            (Timer timer) => setState(
              () {
            if (timeExpired ==0) {
              isOtpExpire = true;
              seconds = "00";
              timer.cancel();
            } else {
              minute = strFormat.format((timeExpired / 60).floor());
              seconds = strFormat.format((timeExpired % 60).floor());
              timeExpired -= 1;
              isOtpExpire = false;
            }
          },
        ),
      );
  }

  _sendSms() async {
    Utils().showLoading();
    if(isShowOtp){
      _signInWithPhoneNumber();
      return;
    }
    print("run sms::::::::::::::::::::");
//
    final PhoneVerificationCompleted verificationCompleted =
        (AuthCredential phoneAuthCredential) async {
      Get.back();
          print('PhoneVerificationCompleted:::::::');
      // Todo After Verification Complete
    };
//
    final PhoneVerificationFailed verificationFailed =
        (authException) {
          Get.back();
      print('Auth Exception is::::::: ${authException.message}');
      Get.defaultDialog(title: "Error",content: Text("PhoneVerificationFailed"));
    };
//
    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
          Get.back();
      print('verification id is:::::: $verificationId');
      _verificationId = verificationId;
      startTimer();
      setState(() {
        isShowOtp = true;
      });
    };
//
    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
          Get.back();
          print("codeAutoRetrievalTimeout::::::::::::::::::::");
          Get.defaultDialog(title: "Error",content: Text("codeAutoRetrievalTimeout"));
          setState(() {
            isShowOtp = false;
          });
    };
//
    await _auth.verifyPhoneNumber(
        phoneNumber: "+84${_phoneController.text}",
        timeout: const Duration(seconds: 60),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
    print("run sms complete::::::::::::::::::::");

  }



  // Example code of how to sign in with phone.
  Future<void> _signInWithPhoneNumber() async {
    try {
      final PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: _verificationId,
        smsCode: _otpController.text,
      );
      final User user = (await _auth.signInWithCredential(credential)).user;
      Get.back();
      Get.find<LoginController>().gotoBlankScreen(
          title: "Home", content: "Successfully signed in UID: ${user.uid}");
    } catch (e) {
      Get.back();
      print(e);
      Get.defaultDialog(title: "Failed to sign in",content: Text("${e.toString()}"));
    }
  }
}