import 'package:demo_app/controller/endpoint_controller.dart';
import 'package:demo_app/model/login_lesponse.dart';
import 'package:demo_app/page/blank_screen/blank_screen_controller.dart';
import 'package:demo_app/servies/login_provider.dart';
import 'package:demo_app/utils/strings.dart';
import 'package:demo_app/utils/utils.dart';
import 'package:get/get.dart';

class LoginController extends GetxController{
  login({String email, String password}) async {
    Utils().showLoading();
    var loginResponse = await LoginProvider().login(email: email, password: password);
    Get.back();
    if(loginResponse != null) {
      if(loginResponse.code == 100) {
        Get.find<InfoController>().loginResponse.value = loginResponse;
        gotoBlankScreen(title: "Home",content: loginResponse.toString());
      }else{
        Utils().showPopupErrorMessage(message: loginResponse.message);
      }
    }else{
      Utils().showPopupErrorMessage(message: something_went_wrong);
    }
  }

  gotoBlankScreen({String title, String content}){
    Get.find<BlankScreenController>().title = title;
    Get.find<BlankScreenController>().content = content;
    Get.toNamed(BLANK_SCREEN);
  }
}