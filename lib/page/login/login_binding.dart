import 'package:demo_app/controller/endpoint_controller.dart';
import 'package:demo_app/page/blank_screen/blank_screen_controller.dart';
import 'package:get/get.dart';

import 'login_controller.dart';

class LoginBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController());
    Get.lazyPut(() => BlankScreenController());
  }
}