
import 'package:demo_app/page/blank_screen/blank_screen.dart';
import 'package:demo_app/page/blank_screen/blank_screen_binding.dart';
import 'package:demo_app/page/first_screen/first_screen.dart';
import 'package:demo_app/page/first_screen/firstscreen_binding.dart';
import 'package:demo_app/page/home/home_bindings.dart';
import 'package:demo_app/page/home/home_screen.dart';
import 'package:demo_app/page/login/login_binding.dart';
import 'package:demo_app/page/login/login_screen.dart';
import 'package:demo_app/page/video_picker_page/video_picker_page.dart';
import 'package:demo_app/utils/strings.dart';
import 'package:get/get.dart';

GetMaterialApp router(){
  return GetMaterialApp(
    initialRoute: "/",
    getPages: [
      GetPage(name: '/', page: () => HomeScreen(), binding: HomeBindings()),
    ],
  );
}