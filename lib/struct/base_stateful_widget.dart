import 'package:demo_app/utils/color.dart';
import 'package:flutter/material.dart';

abstract class BaseStatefulWidgetState<statefulWidget extends StatefulWidget> extends State<statefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: buildWidget(),
      ),
    );
  }

  Widget buildWidget();
  String appBarTitle();
  bool isCanGoBack();
}
