import 'package:demo_app/controller/endpoint_controller.dart';
import 'package:demo_app/model/login_lesponse.dart';
import 'package:demo_app/utils/url.dart';
import 'package:get/get.dart';

class LoginProvider extends GetConnect {
  Future<LoginResponse> login({String email, String password}) async {
    String endpoint = Get.find<InfoController>().endpointUrl.value;
    Map data = {
      "email": email ?? "",
      "password": password ?? "",
      "account_id": 0,
      "device_type": "android",
      "uiid": 123,
    };

    var response = await post("$endpoint$LOGIN", data);

    if (response.statusCode == 200) {
      var result = LoginResponse.fromJson(response.body);
      return result;
    }
    return null;
  }
}
