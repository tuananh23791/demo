import 'package:demo_app/model/endpoint.dart';
import 'package:demo_app/utils/url.dart';
import 'package:get/get.dart';

class GetEndpointProvider extends GetConnect{

  Future<Endpoint> getEndpoint(String basicAuth, ) async {
    var response = await get("$GET_ENDPOINT?v=1.0.1&e=ios",headers: {'authorization': basicAuth});
    if(response.statusCode == 200){
      var endpoint = Endpoint.fromJson(response.body);
      return endpoint;
    }
    return null;
  }

}