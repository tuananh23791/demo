import 'package:demo_app/model/login_lesponse.dart';
import 'package:demo_app/servies/get_endpoint_provider.dart';
import 'package:demo_app/utils/strings.dart';
import 'package:demo_app/utils/utils.dart';
import 'package:get/get.dart';

class InfoController extends GetxController{
  var endpointUrl = "".obs;
  var loginResponse = LoginResponse().obs;
  var status = "".obs;
  GetEndpointProvider getEndpointProvider = GetEndpointProvider();

  getEndPoint() async {
    Utils().showLoading();

    var endpoint = await getEndpointProvider.getEndpoint(Utils().getAuth());
    Get.back();
    if(endpoint != null){
      status.value = "load successfully.";
      endpointUrl.value = endpoint.endpoint;
      Get.offNamed(LOGIN_SCREEN);
    }else{
      status.value = "load endpoint failed...";
    }
  }
}